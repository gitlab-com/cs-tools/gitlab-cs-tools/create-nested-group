# Create nested group

Create a nested group from path, i.e. creates all groups (if not exists), in the path /topgroup/subgroup/subgroup

Part of the path may already exist, so this can be used to create deep subgroups in existing groups.

# Usage:

`pip3 install -r requirements.txt`
`python3 create_nested_group.py $gitlab_instance $GIT_TOKEN $group`

Parameters:
* gitlab_instance: The GitLab instance this script should run on
* GIT_TOKEN: An API token for the instance. May need an admin token if group creation is restricted on your instance. May need a group owner token if you want to create nested subgroups in an existing group.
* group: The full path of group to create, i.e. /topgroup/subgroup/subgroup. The path always as to start with a root level group