#!/usr/bin/env python3

import gitlab
import argparse
import csv

'''
Creates a nested group from path, i.e. creates all groups if not exist, given the path /topgroup/subgroup/subgroup..
Usage:
python3 create_nested_group.py $gitlab_instance $GIT_TOKEN $group

Parameters:
* gitlab_instance: The GitLab instance this script should run on
* GIT_TOKEN: An admin token for the instance
* group: The full path of group to create

'''

def create_top_group(gl, name):
    try:
        top_group = gl.groups.create({'name': name, 'path': name}, retry_transient_errors=True)
        return top_group.id
    except gitlab.exceptions.GitlabCreateError as err:
        print("[Info] Top-level group already exists: %s" % name)
        top_group = gl.groups.get(name, retry_transient_errors=True)
        return top_group.id
    except gitlab.exceptions.GitlabHttpError as err:
        print("[ERROR] Can not create top-level group.")
        print(err)
        exit(1)

# ugly recursive method with too much copy and pasting
def create_group_copy(gl, path, parent_id, fullpath=""):
    if not path:
        return
    # get the current subgroup we want to create by splitting, then taking first element
    subgroups = path.strip("/").split("/")
    try:
        sub_group = gl.groups.create({'name': subgroups[0], 'path': subgroups[0], 'parent_id':parent_id} , retry_transient_errors=True)
        fullpath += "/" + subgroups[0]
        print("[INFO] Created group %s" % fullpath)
        del subgroups[0]
        create_group_copy(gl, "/".join(subgroups), sub_group.id, fullpath.strip("/"))
    except gitlab.exceptions.GitlabCreateError as err:
        fullpath += "/" + subgroups[0]
        print("[Info] Group already exists: %s" % fullpath)
        sub_group = gl.groups.get(fullpath , retry_transient_errors=True)
        del subgroups[0]
        create_group_copy(gl, "/".join(subgroups), sub_group.id, fullpath.strip("/"))
    except gitlab.exceptions.GitlabHttpError as err:
        print("[ERROR] Can not create group %s" % subgroups[0])
        print(err)
        exit(1)

parser = argparse.ArgumentParser(description='Transfer projects into different top group structure')
parser.add_argument('gitlab', help='GitLab URL')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('group', help='Full path of group to create')
args = parser.parse_args()

gitlaburl = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
gl = gitlab.Gitlab(gitlaburl, private_token = args.token)

group = args.group.strip("/")

top_group = group[0 : group.find("/")]
top_group_id = create_top_group(gl, top_group)

subgroup_path = group[group.find("/")+1:]

create_group_copy(gl, subgroup_path, top_group_id, top_group)
